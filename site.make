core = 7.x
api = 2

;  uw_ft_grad_program_search
projects[uw_ft_program_search][type] = "module"
projects[uw_ft_program_search][download][type] = "git"
projects[uw_ft_program_search][download][url] = "https://git.uwaterloo.ca/mur-dev/uw_ft_grad_program_search.git"
projects[uw_ft_program_search][download][tag] = "7.x-1.1"
projects[uw_ft_program_search][subdir] = ""

;  uw_ct_grad_program_search
projects[uw_ct_program_search][type] = "module"
projects[uw_ct_program_search][download][type] = "git"
projects[uw_ct_program_search][download][url] = "https://git.uwaterloo.ca/mur-dev/uw_ct_grad_program_search.git"
projects[uw_ct_program_search][download][tag] = "7.x-1.1"
projects[uw_ct_program_search][subdir] = ""